#!/usr/bin/env sh


set -eu


# Sleep 5 minutes so a crash looping pod doesn't punch let's encrypt.
echo "Renewing cert in 5 minutes"
sleep 300

/generate-cert.sh
