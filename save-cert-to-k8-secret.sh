#!/usr/bin/env sh


set -eu


: "${CERT_NAME:?Please set the CERT_NAME environment variable}"
: "${DOMAINS:?Please set the DOMAINS environment variable}"
: "${SECRET_NAME:?Please set the SECRET_NAME environment variable}"


echo "Saving certificate ${CERT_NAME} for domains ${DOMAINS} into ${SECRET_NAMESPACE}/${SECRET_NAME}"

kubectl apply -f - <<EOF
kind: Secret
apiVersion: v1
metadata:
  namespace: "${SECRET_NAMESPACE}"
  name: "${SECRET_NAME}"
type: Opaque
data:
  tls.crt: "$(cat /etc/letsencrypt/live/${CERT_NAME}/fullchain.pem | base64 | tr -d '\n')"
  tls.key: "$(cat /etc/letsencrypt/live/${CERT_NAME}/privkey.pem | base64 | tr -d '\n')"
EOF
