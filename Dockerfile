ARG IMAGE

FROM ${IMAGE}
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ARG PLATFORM

ENV KUBECTL_VERSION="v1.13.2"
ENV ACME_SERVER="https://acme-v02.api.letsencrypt.org/directory"
ENV ACME_DNS_SERVER="https://auth.acme-dns.io"
ENV SECRET_NAMESPACE="default"


COPY run.sh /run.sh
COPY generate-cert.sh /generate-cert.sh
COPY acme-dns-auth-hook.sh /acme-dns-auth-hook.sh
COPY save-cert-to-k8-secret.sh /save-cert-to-k8-secret.sh
COPY cross-build-* /usr/bin/


RUN ["cross-build-start"]

RUN apk add --no-cache --update curl certbot ca-certificates jq && \
	curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/${PLATFORM}/kubectl && \
	chmod +x kubectl /run.sh /generate-cert.sh /acme-dns-auth-hook.sh /save-cert-to-k8-secret.sh && \
	mv kubectl /usr/local/bin/kubectl

RUN ["cross-build-end"]


VOLUME ["/etc/letsencrypt"]
CMD ["/run.sh"]
